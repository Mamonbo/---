/*
 * GccApplication2.c
 *
 * Created: 2014/08/12 17:51:15
 * Author: 食べ太郎
 * PWMで
 * サーボを
 * 制御する
 * 説明
 * 講習会のボードではサーボ用は15番　OC1Aなので
 * timer1を使うこと確定
 * timer1はatmega168Pの目玉機能なようで16bitのカウンタを持つ
 */ 

#define F_CPU   (1E6)
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
void delay_ms(int i);
void savo_angle(int8_t kakudo);
int8_t savo_getangle(void);
void savo_turn(int16_t kakudo);

#define TOP_VELUE   16000//1MHz下で周期16ms
#define KKAKUDO 500
#define SYUKI 1500



void delay_ms(int i){
	while(i--)
	_delay_ms(1);
}

void init(){
	//全ポート入力設定
	DDRB = DDRC = DDRD = 0x00;
	//全部プルアップ設定
	PORTB = PORTC = PORTD = 0xFF;
	//LEDにつながっているピンを出力に
	DDRD |= 0b00001111;
	
	DDRB |= 0b00000010;//15番PB1を出力に
	//ここからタイマ設定＆タイマ割り込み有効化
	//割り込み禁止
	cli();
	//タイマ/カウンタ1(レジスタ)(WORD)：カウンタを0にする
	TCNT1 = 0;
	//タイマ/カウンタ1比較Aレジスタ(WORD)    これをTCNT1の最大値として使う
	OCR1A = TOP_VELUE - 1;
	//タイマ/カウンタ1比較Bレジスタ(WORD)    これをTCNT1との比較値として使う
	OCR1B = 0;//とりあえず脱力状態
	//タイマ/カウンタ1制御レジスタA：
	//  OCR1AをTOP値とする高速PWM動作にする　OCR1B比較一致でLow,BOTTOMでHi
	//  (1<<COM1B1)でタイマとOCR1Bのピンがつながる
	//  ちなみにOC1Bは通常運転させてモータドライバの片割れとして使うような設計
	TCCR1A |= (1<<COM1A1)|(0<<COM1A0)|(0<<COM1B1)|(0<<COM1B0);
	
	//0-OCR1B Hi OCR1B-OCR1B LoなPWM設定
	TCCR1A |= (1<<WGM11)|(1<<WGM10);
	TCCR1B |= (1<<WGM13)|(1<<WGM12);
	
	//prescaler=1/1 16bitにゴリ押し
	TCCR1B |= (0<<CS12) |(0<<CS11) |(0<<CS10);
	sei();
}
int main(void)
{
    while(1)
    {
		savo_angle(-90);
		delay_ms(3000);
        savo_angle(90);
		delay_ms(3000);
    }
}

void savo_angle(int8_t kakudo)
{
	int16_t tmp;
	if(kakudo<-90)
	{
		kakudo=-90;
	}
	else if(kakudo>90)
	{
		kakudo=90;
	}
	//	OCR1B=(1.5+0.5*(kakudo/90))*1000;
	tmp=KKAKUDO/10*kakudo/9;
	OCR1B=tmp+SYUKI;
}
int8_t savo_getangle(void)
{
	int16_t tmp=(OCR1B-SYUKI)*9/(KKAKUDO/10);
	return (int8_t) tmp;
}
void savo_turn(int16_t kakudo)
{
	if(kakudo<-180)
	{
		kakudo=-180;
	}
	else if (kakudo>180)
	{
		kakudo=180;
	}
	kakudo+=savo_getangle();
	if(kakudo<-90)
	{
		kakudo=-90;
	}
	else if(kakudo>90)
	{
		kakudo=90;
	}
	savo_angle(kakudo);
}