/*
 * GccApplication3.c
 *
 * Created: 2014/09/17 19:02:55
 *  Author: 食べ太郎
 */ 

#define F_CPU (1E6)
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

void delay_ms(int i)
{
	while(i--)
		_delay_ms(1);
}


void savo_angle(int8_t pin,int8_t kakudo);
int8_t savo_getangle(int8_t pin);
void savo_turn(int8_t pin,int16_t kakudo);

#define OC0A 1
#define OC0B 2
#define OC1A 3
#define OC1B 4
#define OC2A 5
#define OC2B 6
void savo_on(int8_t pin);
void savo_off(int8_t pin);

void savo_free(int8_t pin);

#define TOP_VELUE_16   16000//1MHz下で周期16ms
#define KKAKUDO_16 500
#define SYUKI_16 1500


#define TOP_VELUE_8   250//1MHz下で周期16ms
#define KKAKUDO_8 500/64
#define SYUKI_8 1500/64


void init(){
	//全ポート入力設定
	DDRB = DDRC = DDRD = 0x00;
	//全部プルアップ設定
	PORTB = PORTC = PORTD = 0xFF;

	//PWM周りを出力に
	DDRB |= 0b00000110;
	DDRD |= 0b01001000;

	//ここからタイマ設定＆タイマ割り込み有効化
	//割り込み禁止
	cli();
	//タイマ/カウンタ1(レジスタ)(WORD)：カウンタを0にする
	TCNT1 = 0;
	//タイマ/カウンタ1比較Aレジスタ(WORD)    これをTCNT1の最大値として使う
	ICR1 = TOP_VELUE_16 - 1;
	//タイマ/カウンタ1比較Bレジスタ(WORD)    これをTCNT1との比較値として使う
	OCR1A = 0;//とりあえず脱力状態
	OCR1B = 0;//とりあえず脱力状態
	
	//0-OCR1B Hi OCR1B-OCR1B LoなPWM設定
	TCCR1A |= (1<<WGM11)|(0<<WGM10);
	TCCR1B |= (1<<WGM13)|(1<<WGM12);
	
	//prescaler=1/1 16bitにゴリ押し
	TCCR1B |= (0<<CS12) |(0<<CS11) |(0<<CS10);
	
	TCNT0=0;
	OCR0A=TOP_VELUE_8-1;
	OCR0B=0;
	//fast PWM mode TOP:OCR0A CMP:OCR0B
	TCCR0A |= (1<<WGM01)|(1<<WGM00);
	TCCR0B |= (1<<WGM02);
	//prescaler 1/64
	TCCR0B |= (1<<CS01)|(1<<CS00);

	TCNT2=0;
	OCR2A=TOP_VELUE_8-1;
	OCR2B=0;
	//fast PWM mode TOP:OCR2A CMP:OCR2B
	TCCR2A |= (1<<WGM21)|(1<<WGM20);
	TCCR2B |= (1<<WGM22);
	//prescaler 1/64
	TCCR2B |= (1<<CS21)|(1<<CS20);
	
	
	sei();
	savo_on(OC1A);//二の腕サーボon
	savo_on(OC1B);//手首サーボor磁石板サーボ on
	savo_angle(OC1A,80);
	savo_angle(OC1B,0);
	
}
int main(void)
{
	int sak=~PORTD & 0b00000011;
	init();
	if(sak==0)
	{
		PORTD |= 0b01001000;
		while(PORTD & 0x04)
		{
			delay_ms(100);
		}
		PORTD &= ~(0b01001000);//モーター停止
		savo_angle(OC1A,-80);
		delay_ms(4000);
		savo_angle(OC1A,-45);
	}
	else if (sak==1)
	{
		PORTD |= 0b01001000;
		while(PORTD & 0x04)
		{
			delay_ms(100);
		}
		PORTD &= ~(0b01001000);//モーター停止
		savo_angle(OC1B,90);
	}
	else if(sak==2)
	{
		savo_angle(OC1B,90);
		PORTD |= 0b01001000;
		while(PORTD & 0x04)
		{
			delay_ms(100);
		}
		PORTD &= ~(0b01001000);//モーター停止
		savo_angle(OC1B,-90);
		//ココらへんにうろつくコードでも
	}
    while(1)
    {
        //TODO:: Please write your application code
		//そしてatmega168Pは考えるのをやめた
    }
}


void savo_angle(int8_t pin,int8_t kakudo)
{
	int16_t tmp;
	if(kakudo<-90)
	{
		kakudo=-90;
	}
	else if(kakudo>90)
	{
		kakudo=90;
	}
	
	//	OCR1A=(1.5+0.5*(kakudo/90))*1000;
	//  OCR0B=(1.5+0.5*(kakudo/90))*1000/64;
	if(pin==OC0A || pin==OC0B)
	{
		tmp=KKAKUDO_16/10*kakudo/9;
		tmp=(tmp+SYUKI_16)/64;
		OCR0B=(int8_t) tmp;
	}
	else if(pin==OC1A)
	{
		tmp=KKAKUDO_16/10*kakudo/9;
		OCR1A=tmp+SYUKI_16;
	}
	else if(pin==OC1B)
	{
		tmp=KKAKUDO_16/10*kakudo/9;
		OCR1B=tmp+SYUKI_16;
	}
	else if (pin==OC2A||pin==OC2B)
	{
		tmp=KKAKUDO_16/10*kakudo/9;
		tmp=(tmp+SYUKI_16)/64;
		OCR2B=(int8_t) tmp;
	}


}
int8_t savo_getangle(int8_t pin)
{
	int16_t tmp;
	if(pin==OC0A || pin==OC0B)
	{
		tmp=OCR0B;
		tmp*=64;
	}
	else if(pin == OC1A)
	{
		tmp=OCR1A;
	}
	else if(pin==OC1B)
	{
		tmp=OCR1B;
	}
	else if(pin==OC2A || pin==OC2B)
	{
		tmp=OC2B;
		tmp*=64;
	}
	else
	{
		return -1;
	}
	tmp=(tmp-SYUKI_16)*9/(KKAKUDO_16/10);
	return (int8_t) tmp;
}
void savo_turn(int8_t pin,int16_t kakudo)
{
	if(kakudo<-180)
	{
		kakudo=-180;
	}
	else if (kakudo>180)
	{
		kakudo=180;
	}
	if(pin<0 || pin>6)
	{
		return;
	}
	
	kakudo+=savo_getangle(pin);
	if(kakudo<-90)
	{
		kakudo=-90;
	}
	else if(kakudo>90)
	{
		kakudo=90;
	}
	savo_angle(pin,kakudo);
}

void savo_on(int8_t pin)
{
  /*
   *サーボ用PWMを設定
   *設定後はとりあえず脱力
   */
  cli();
  if(pin==OC0A)
    {
		TCCR0A |= (1<<COM0A1);
    }
  else if(pin==OC0B)
    {
		TCCR0A |= (1<<COM0B1);
    }
  else if(pin==OC1A)
    {
		TCCR1A |= (1<<COM1A1);
    }
  else if(pin==OC1B)
    {
		TCCR1A |= (1<<COM1B1);
    }
  else if(pin==OC2A)
    {
		TCCR2A |= (1<<COM2A1);
    }
  else if(pin==OC2B)
    {
		TCCR2A |= (1<<COM2B1);
    }
  sei();
}

void savo_off(int8_t pin)
{
  cli();
  if(pin==OC0A)
    {
		TCCR0A = TCCR0A & (~((1<<COM0A1)|(1<<COM0A0)));
    }
  else if(pin==OC0B)
    {
		TCCR0A = TCCR0A & (~((1<<COM0B1)|(1<<COM0B1)));
    }
  else if(pin==OC1A)
    {
		TCCR1A = TCCR1A & (~((1<<COM1A1)|(1<<COM1A0)));
    }
  else if(pin==OC1B)
    {
		TCCR1A = TCCR1A & (~((1<<COM1B1)|(1<<COM1B0)));
    }
  else if(pin==OC2A)
    {
		TCCR2A = TCCR2A & (~((1<<COM2A1)|(1<<COM2A0)));
    }
  else if(pin==OC2B)
    {
		TCCR2A = TCCR2A & (~((1<<COM2B1)|(1<<COM2B0)));
    }
  sei();
}

void savo_free(int8_t pin)
{
	//指定したサーボを脱力させる
	if(pin==OC0A || pin==OC0B)
	{
		OCR0B=0;
	}
	else if(pin==OC1A)
	{
		OCR1A=0;
	}
	else if(pin==OC1B)
	{
		OCR1B=0;
	}
	else if (pin==OC2A||pin==OC2B)
	{
		OCR2B=0;
	}
}
