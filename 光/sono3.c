#include <Servo.h>

Servo nino;
Servo gelenk;

void setup()
{
  //作戦、衝突検出用
  pinMode(2,INPUT);
  pinMode(3,INPUT);
  pinMode(4,INPUT);

  //モーター
  pinMode(5,OUTPUT);
  pinMode(12,OUTPUT);
  
  //お知らせ用LED
  pinMode(11,OUTPUT);
  //サーボ
  pinMode(15,OUTPUT);
  pinMode(16,OUTPUT);

  nino.attach(15);
  gelenk.attach(16);
  nino.write(10);
  gelenk.attach(0);
}

void loop()
{
  //ループのしないloop()関数
  int sak=0;
  if(degitalRead(2)==HIGH)
    sak += 1;
  if(degitalRead(3)==HIGH)
    sak += 2;

  if(sak==0)
    {
      degitalWrite(5,HIGH);
      degitalWrite(12,HIGH);
      while(degitalRead(4)==LOW)
	{
	  delay(100);
	}
      degitalWrite(5,LOW);//モーター停止
      degitalWrite(12,LOW);

      nino.write(170);
      delay(4000);
      nino.write(135);
    }
  else if(sak==1)
    {
      degitalWrite(5,HIGH);
      degitalWrite(12,HIGH);
      while(degitalRead(4)==LOW)
	{
	  delay(100);
	}
      degitalWrite(5,LOW);//モーター停止
      degitalWrite(12,LOW);
      /*後退のコードを書くならここに*/
      gelenk.write(120);//缶を剥がす
    }
  else if(sak==2)
    {
      degitalWrite(5,HIGH);
      degitalWrite(12,HIGH);
      while(degitalRead(4)==LOW)
	{
	  delay(100);
	}
      degitalWrite(5,LOW);//モーター停止
      degitalWrite(12,LOW);

      gelenk.write(180);//レア缶から排出(出来るのかなあ)
    }

  while(1)
    {
      //最終形態　待機
      digitalWrite(11,HIGH);
      delay(300);
      digitalWrite(11,LOW);
      delay(300);
    }
}
